const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const PORT = 3000;


app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

io.on('connection', (socket) => {
  let user;
  socket.on('join', (name) => {
    user = name;
    console.log(`${name} : has joined`);
    socket.broadcast.emit('join', `${name} has joined`);
  });
  socket.on('typing', (name) => {
    socket.broadcast.emit('typing', name);
  })
  socket.on('not typing', (name) => {
    socket.broadcast.emit('not typing', name);
  })
  socket.on('chat message', ({ name, msz }) => {
    socket.broadcast.emit('chat message',{ name , msz });
  })
  socket.on('disconnect', () => {
    console.log('User left');
    io.emit('left', `${user} : has left`);
  })
})

http.listen(PORT, () => {
  console.log(`Listening to Port ${PORT}`);
});